const AWS = require('aws-sdk'); 
const dynamoDB = new AWS.DynamoDB.DocumentClient();

const { getSeason, giftParams } = require('./utils');


module.exports.createGift = async(event) => {

    console.info({event});

    const data = JSON.parse(event.Records[0].body);
    const body = JSON.parse(data);
    
    const month = new Date(body.birthDate).getMonth();
    const gift = getSeason(month);    
    const dbParams = giftParams({dni: body.dni, gift});
    try {
        await dynamoDB.update(dbParams).promise();
    } catch (error) {
        console.error(`Error updating gift: ${error}`);
        return {"statusCode": 400, "body": JSON.stringify({error:error.message})};
    }

    return {
        statusCode: 200,
        body: JSON.stringify({message: 'Gift Created!'})
    }    

}