const getSeason = (month) => {
    if (3 <= month && month <= 5)  return 'Sweater' // autumn
    if (6 <= month && month <= 8)  return 'Buzo'   // winter
    if (9 <= month && month <= 12) return'Camisa' // spring    
    return 'Remera'; // summer
}

const giftParams = (data) => {    

    const params = {

        TableName: process.env.CLIENT_TABLE,
        Key: {
            dni: data.dni
        },
        UpdateExpression: 'set #gift = :gi',
        ExpressionAttributeNames: {
            '#gift': 'gift',
        },
        ExpressionAttributeValues: {
            ':gi': data.gift,
        },
        ReturnValues: "ALL_NEW",       
    }

    return params;
}

module.exports = {
    getSeason,
    giftParams
}