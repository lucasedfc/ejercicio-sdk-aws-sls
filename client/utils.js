const minAge = 18;
const maxAge = 65;

const calculateAge = (birthDate) => {
    const ageDiff = Date.now() - new Date(birthDate).getTime()
    const ageDiffToDateFormat = new Date(ageDiff);
    return Math.abs((ageDiffToDateFormat.getUTCFullYear() -1970));    
}

const validateParams = (data) => {
    
    if(!data.firstName || !data.lastName || !data.birthDate || !data.dni) {
        throw new Error("Missing Parameters!");
    }

    const age = calculateAge(data.birthDate);

    if(age < minAge || age > maxAge) {
        throw new Error("Your age is not allowed for this operation")
    }

    return data;    
}

const itemParams = (data) => {
    const Item = {
        dni: data.dni,
        firstName:  data.firstName,
        lastName: data.lastName,
        birthDate: data.birthDate
    }

    const params = {
        Item,
        TableName: process.env.CLIENT_TABLE,
    }

    return params;
}

module.exports = {
    validateParams,
    itemParams
}