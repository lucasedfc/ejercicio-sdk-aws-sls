const AWS = require('aws-sdk'); 
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const sns = new AWS.SNS();
const { validateParams, itemParams } = require('./utils');

module.exports.createClient = async (event) => {    
    
    console.log(`Event Data: ${JSON.stringify(event)}`);    

    try {
        const client = validateParams(JSON.parse(event.body));
        const dbParams = itemParams(client);
        await dynamoDB.put(dbParams).promise();
        await sns.publish({
            Message: JSON.stringify(event.body),
            TopicArn: process.env.CLIENT_TOPIC
        }).promise()

    } catch (error) {
        console.error(`Error creating client: ${error}`);
        return {"statusCode": 400, "body": JSON.stringify({error:error.message})};
    }
    
    return {
        statusCode: 200,
        body: JSON.stringify({message: 'Client Created!'})
    }
}