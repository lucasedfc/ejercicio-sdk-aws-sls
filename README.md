# Academy AWS Serverless Exercise

Serverless Application to sign-up users, assign a credit card (gold or classic) and a gift.

# Stack

Lambdas:

- clientCreate: Create clients with DNI, firstName, lastName and birthDate. It should only allow adults (+18) up to 65 years of age.
- clientCard: Grant a credit card (Classic if you are under 45 years old and Gold if you are older) generating a random number, expiration date and security code.
- clientGift: Assign a gift according to the season that corresponds to the customer's birthday date; summer: t-shirt, autumn: sweater, winter: sweatshirt, spring: shirt

SQS:

- cardQueue
- giftQueue

SNS

- clientsTopic
- GiftSubscription
- CardSubscription

DynamoDB

- ClientsDynamoDbTable

API Gateway

- /clients POST

IAM Policies

- snsToGiftQueueSqsPolicy
- snsTocardQueueSqsPolicy


## Dependencies

Need to install the dependency serverless-iam-roles-per-function

```javascript
npm install
```

To be able to deploy the application it is necessary to install serverless

```javascript
npm i serverless
```

- AWS Credentials


## Deploy

Configure your AWS credentials (.aws/credentials)

To be able to deploy the application it is necessary to install serverless

```javascript
npm i serverless
```

```javascript
serverless deploy --verbose
```

## Body

Required fields:

```javascript
{
    "dni":"12345678",
    "firstName": "Michael",
    "lastName": "Scott",
    "birthDate": "1990-03-27"
}
```