const calculateAge = (birthDate) => {
    const ageDiff = Date.now() - new Date(birthDate).getTime()
    const ageDiffToDateFormat = new Date(ageDiff);
    return Math.abs((ageDiffToDateFormat.getUTCFullYear() -1970));    
}

const cardParams = (data) => {    

    const params = {

        TableName: process.env.CLIENT_TABLE,
        Key: {
            dni: data.dni,
        },
        UpdateExpression: 'set #cardNumber = :cn, #expirationDate = :ex, #securityCode = :sc, #cardType = :ct',
        ExpressionAttributeNames: {
            '#cardNumber': 'cardNumber',
            '#expirationDate': 'expirationDate',
            '#securityCode': 'securityCode',
            '#cardType': 'cardType'
        },
        ExpressionAttributeValues: {
            ':cn': data.cardNumber,
            ':ex': data.expirationDate,
            ':sc': data.cardCode,
            ':ct': data.type
        },
        ReturnValues: "ALL_NEW",       
    }

    return params;
}

const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min); 
}

module.exports = { 
    calculateAge,
    randomNumber,
    cardParams 
}